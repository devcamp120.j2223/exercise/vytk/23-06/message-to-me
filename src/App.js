import 'bootstrap/dist/css/bootstrap.min.css'
import ContentComponent from './components/content/contentComponent';
import TitleComponent from './components/titles/titleComponent';
function App() {
  return (
    <div className='container'>
      <TitleComponent />
      <ContentComponent />
    </div>
  );
}

export default App;
