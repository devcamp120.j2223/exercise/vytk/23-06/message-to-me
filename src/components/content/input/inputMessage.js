import { Component } from "react";

class inputMessage extends Component{
    inputChangeHandler=(event)=> {
        console.log("Giá trị input thay đổi")
        console.log(event.target.value)
        this.props.changeInputMessageProp(event.target.value)
    }

    buttonClickHandler=()=> {
        console.log("Nút được bấm")
        this.props.changeOutputMessageProp();
    }
    render(){
        return (      
            <div>
                <div className='row'>
                    <div className='col-sm-12'>
                        <input className='form-control' placeholder={this.props.messageInputProp} onChange={this.inputChangeHandler}/>
                    </div>
                </div>
                <div className='row'>
                    <div className='col-sm-12'>
                        <button className='btn btn-success' onClick={this.buttonClickHandler}> Gửi thông điệp</button>
                    </div>
                </div>
            </div>
      )
    }
}

export default inputMessage;