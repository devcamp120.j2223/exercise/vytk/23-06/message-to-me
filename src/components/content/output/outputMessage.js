import { Component } from "react";
import like from '../../../assets/images/like.png'
class outputMessage extends Component{
    render(){
        return (      
            <div>
                <div className='row'>
                    <div className='col-sm-12'>
                        {
                            this.props.messageOutputProp.map((value,index)=>{
                                return <p key={index}>{value}</p>
                            })
                        }
                    </div>
                </div>
                {
                    this.props.likeImgProp ? 
                    <div className='row'>
                        <div className='col-sm-12'>
                            <img src={like} alt='img-like' width={100} />
                        </div>
                    </div>
                    : null
                }
            </div>
      )
    }
}

export default outputMessage;