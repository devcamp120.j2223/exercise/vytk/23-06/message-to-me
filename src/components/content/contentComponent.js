import { Component } from "react";
import InputMessage from "./input/inputMessage";
import OutputMessage from "./output/outputMessage";
class contentComponent extends Component{
    constructor(props){
        super(props);
        this.state={
            messageInput:"Nhập message",
            messageOutPut:[],
            likeImg:false
        }
    }
    changeInputMessageHandler = (value) => {
        console.log(value);
        this.setState({
            messageInput: value
        })
    }
    changeOutputMessageHandler = () => {
        this.setState({
            messageOutPut:[...this.state.messageOutPut,this.state.messageInput],
            likeImg:this.state.messageInput?true:false
        })
    }
    render(){
        return(
            <div>
                <InputMessage messageInputProp={this.state.messageInput} changeInputMessageProp={this.changeInputMessageHandler}  changeOutputMessageProp={this.changeOutputMessageHandler}/>
                <OutputMessage messageOutputProp={this.state.messageOutPut} likeImgProp={this.state.likeImg}/>
            </div>
        )
    }
}

export default contentComponent;