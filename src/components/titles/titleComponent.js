import { Component } from "react";
import TitleImage from "./images/titleImage";
import TitleText from "./text/titleText";
class titleComponent extends Component{
    render(){
        return(
            <div>
                <TitleText/>
                <TitleImage/>
            </div>
        )
    }
}

export default titleComponent;