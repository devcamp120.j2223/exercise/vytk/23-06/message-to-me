import { Component } from "react";
import background from '../../../assets/images/background.png'
class titleImage extends Component{
    render(){
        return (      
        <div className='row'>
            <div className='col-sm-12'>
              <img src={background} alt='background image' width={500} className='img-thumbnail' />
            </div>
        </div>
      )
    }
}

export default titleImage;